package pkg.dev.homulacrm.rest;


import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;



public class HttpHandler {


    private static final String TAG = HttpHandler.class.getSimpleName();
    static String status;

    public String makeServiceCall(String reqUrl) {
        String str;
        StringBuilder stringBuilder;
        String response = null;
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(reqUrl).openConnection();
            conn.setRequestMethod("GET");
            response = convertStreamToString(new BufferedInputStream(conn.getInputStream()));
        } catch (MalformedURLException e) {
            str = TAG;
            stringBuilder = new StringBuilder();
            stringBuilder.append("MalformedURLException: ");
            stringBuilder.append(e.getMessage());
            Log.e(str, stringBuilder.toString());
        } catch (ProtocolException e2) {
            str = TAG;
            stringBuilder = new StringBuilder();
            stringBuilder.append("ProtocolException: ");
            stringBuilder.append(e2.getMessage());
            Log.e(str, stringBuilder.toString());
        } catch (IOException e3) {
            str = TAG;
            stringBuilder = new StringBuilder();
            stringBuilder.append("IOException: ");
            stringBuilder.append(e3.getMessage());
            Log.e(str, stringBuilder.toString());
        } catch (Exception e4) {
            str = TAG;
            stringBuilder = new StringBuilder();
            stringBuilder.append("Exception: ");
            stringBuilder.append(e4.getMessage());
            Log.e(str, stringBuilder.toString());
        }
        return response;
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = reader.readLine();
                String line = readLine;
                if (readLine == null) {
                    break;
                }
                sb.append(line);
                sb.append('\n');
            } catch (IOException e) {
                e.printStackTrace();
                return sb.toString();
            } finally {
                try {
                    is.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    public static String GetJsonFromURL(String data, String weburl) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(weburl);
        stringBuilder.append("?");
        stringBuilder.append(data);
        Log.e("response", stringBuilder.toString());
        BufferedReader reader = null;
        try {
            URLConnection conn = new URL(weburl).openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = reader.readLine();
                String line = readLine;
                if (readLine == null) {
                    break;
                }
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.append(line);
                stringBuilder2.append("\n");
                sb.append(stringBuilder2.toString());
            }
            status = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return status;
        } finally {
            try {
                reader.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return status;
    }
}

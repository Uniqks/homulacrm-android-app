package pkg.dev.homulacrm.rest;

public class apiUrls {

    public static final String BASE_URL = "https://homula.com/smart-realestate/webservices/";
    public static final String LOGIN = "https://homula.com/smart-realestate/webservices/login.php";
    public static final String acceptRejectLead = "https://homula.com/smart-realestate/webservices/accept_reject_lead.php";
    public static final String acceptedLeadList = "https://homula.com/smart-realestate/webservices/accepted_lead_list.php";
    public static final String leadDetail = "https://homula.com/smart-realestate/webservices/lead_detail.php";
    public static final String newLeadList = "https://homula.com/smart-realestate/webservices/new_lead_list.php";
}

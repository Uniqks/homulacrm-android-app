package pkg.dev.homulacrm.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import pkg.dev.homulacrm.R;
import pkg.dev.homulacrm.activity.viewActivity.HomeActivity;
import pkg.dev.homulacrm.app.HomulaApp;
import pkg.dev.homulacrm.databinding.ActivityLoginBinding;
import spencerstudios.com.bungeelib.Bungee;

public class Login extends AppCompatActivity implements View.OnClickListener {


    ActivityLoginBinding binding;
    private HomulaApp mHomulaApp;

    private SharedPreferences mRememberMePref;
    private SharedPreferences.Editor mEditor;
    boolean saveLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        mHomulaApp = (HomulaApp) getApplication();









        initControls();
    }


    /**
     * Method is used to initialized all the controls...
     */
    private void initControls() {


        binding.llTop.setVisibility(View.GONE);
        setAnimationView(binding.llTop);

        binding.loginBT.setOnClickListener(this);

        mRememberMePref = getSharedPreferences("RememberMePref", 0);
        mEditor = mRememberMePref.edit();
        saveLogin = mRememberMePref.getBoolean("saveLogin", false);
        if (saveLogin == true) {
            binding.emailEDT.setText(mRememberMePref.getString("username", ""));
            binding.pwdEDT.setText(mRememberMePref.getString("password", ""));
            binding.rememberCHK.setChecked(true);
        }
    }


    /**
     *  Method is used to initialized set animation...
     */
    public void setAnimationView(View mMainTopLl) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_left_right);
        mMainTopLl.startAnimation(bottomUp);
        mMainTopLl.setVisibility(View.VISIBLE);
    }



    /**
     *  Method is used to initialized check validation...
     */
    private boolean checkValidation() {
        if (!mHomulaApp.isValidEmail(binding.emailEDT)) {
            return false;
        }
        if (!mHomulaApp.isValidatePassword(binding.pwdEDT)) {
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.loginBT:
                mHomulaApp.hideKeyboard(Login.this);
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(findViewById(R.id.loginBT));

                if (checkValidation()){

                    if (binding.rememberCHK.isChecked()) {
                        mEditor.putBoolean("saveLogin", true);
                        mEditor.putString("username", binding.emailEDT.getText().toString());
                        mEditor.putString("password", binding.pwdEDT.getText().toString());
                        mEditor.commit();
                    } else {
                        mEditor.clear();
                        mEditor.commit();
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(Login.this, HomeActivity.class));
                            finish();
                            Bungee.slideLeft(Login.this);
                        }
                    }, 200);
                }
                break;
        }
    }
}

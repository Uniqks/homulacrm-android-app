package pkg.dev.homulacrm.activity.dialog;


import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import pkg.dev.homulacrm.R;
import pkg.dev.homulacrm.databinding.DialogLeadDetailsBinding;


public class LeadDetailsDialog extends DialogFragment implements View.OnClickListener {


    DialogLeadDetailsBinding binding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_lead_details, container, false);

        getDialog().getWindow().requestFeature(1);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.enterDialogAnimation;





        initControls();
        return binding.getRoot();
    }


    /**
     * Method is used to initialized all the controls...
     */
    private void initControls() {

        /*YoYo.with(Techniques.ZoomIn)
                .duration(600)
                .repeat(0)
                .playOn(binding.topRL);*/

        binding.closeRl.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.closeRl:
                getDialog().getWindow().getAttributes().windowAnimations = R.style.outDialogAnimation;
                getDialog().dismiss();
                /*YoYo.with(Techniques.ZoomOut)
                        .duration(600)
                        .repeat(0)
                        .playOn(binding.topRL);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getDialog().dismiss();
                    }
                }, 600);*/
                break;
        }
    }
}

package pkg.dev.homulacrm.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import pkg.dev.homulacrm.R;
import pkg.dev.homulacrm.activity.interfaces.onBackPressed;
import pkg.dev.homulacrm.databinding.FragmentHelpBinding;


public class HelpFragment extends Fragment implements onBackPressed, View.OnClickListener {


    FragmentHelpBinding binding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_help, container, false);







        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls..
     */
    private void initControls() {

        binding.topLL.setVisibility(View.GONE);
        setAnimationView(binding.topLL);


        binding.sendRL.setOnClickListener(this);
        binding.backIV.setOnClickListener(this);
    }

    /**
     *  Method is used to initialized set animation...
     */
    public void setAnimationView(View mMainTopLl) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_left_right);
        mMainTopLl.startAnimation(bottomUp);
        mMainTopLl.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backIV:
                getActivity().getSupportFragmentManager().popBackStack();
                break;

            case R.id.sendRL:
                Toast.makeText(getActivity(), "Work in progress...", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}

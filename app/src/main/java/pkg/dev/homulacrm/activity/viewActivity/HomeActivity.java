package pkg.dev.homulacrm.activity.viewActivity;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import pkg.dev.homulacrm.R;
import pkg.dev.homulacrm.activity.fragment.ChatFragment;
import pkg.dev.homulacrm.activity.fragment.ClientsFragment;
import pkg.dev.homulacrm.activity.fragment.LeadsFragment;
import pkg.dev.homulacrm.activity.fragment.SettingsFragment;
import pkg.dev.homulacrm.databinding.ActivityHomeBinding;

public class HomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {


    ActivityHomeBinding binding;
    boolean doubleBackToExitPressedOnce = false;
    Fragment fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);






        initControls();
    }

    /**
     *  Method is used to initialized all the controls..
     */
    private void initControls() {

        binding.navigationView.getMenu().findItem(R.id.navigation_leads).setChecked(true);
        binding.navigationView.setOnNavigationItemSelectedListener(this);

        Fragment fragment = new LeadsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()) {

            case R.id.navigation_leads:
                getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragment = new LeadsFragment();
                break;

            case R.id.navigation_clients:
                getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragment = new ClientsFragment();
                break;

            case R.id.navigation_chat:
                getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragment = new ChatFragment();
                break;

            case R.id.navigation_settings:
                getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragment = new SettingsFragment();
                break;
        }

        return loadFragment(fragment);
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    public void replaceFragment(Fragment fragment) {

        this.fragment = fragment;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

   /* public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press again to close app", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;

            }
        }, 2000);
    }*/
}

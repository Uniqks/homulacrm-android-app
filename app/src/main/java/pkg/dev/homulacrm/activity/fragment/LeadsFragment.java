package pkg.dev.homulacrm.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pkg.dev.homulacrm.R;
import pkg.dev.homulacrm.activity.adapter.FragmentTabsPagerAdapter;
import pkg.dev.homulacrm.databinding.FragmentLeadsBinding;


public class LeadsFragment extends Fragment {


    FragmentLeadsBinding binding;
    private FragmentTabsPagerAdapter mPagerAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_leads, container, false);





        initControls();
        return binding.getRoot();
    }

    /**
     *  Method is used to initialized all the controls....
     */
    private void initControls() {


        setupViewPager(binding.viewPager);
    }

    /**
     *  setup view pager
     */
    private void setupViewPager(ViewPager viewPager) {
        mPagerAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());

        NewLeadFragment newLeadFragment = new NewLeadFragment();
        mPagerAdapter.addFragment(newLeadFragment, getResources().getString(R.string.newLead));

        ActiveLeadFragment activeLeadFragment = new ActiveLeadFragment();
        mPagerAdapter.addFragment(activeLeadFragment, getResources().getString(R.string.activeLead));



        viewPager.setAdapter(mPagerAdapter);
        binding.leadsTab.setTabMode(TabLayout.MODE_SCROLLABLE);
        binding.leadsTab.setupWithViewPager(viewPager);
        binding.leadsTab.setTabMode(1);
    }


}

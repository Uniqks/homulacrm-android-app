package pkg.dev.homulacrm.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pkg.dev.homulacrm.R;
import pkg.dev.homulacrm.activity.adapter.FragmentTabsPagerAdapter;
import pkg.dev.homulacrm.databinding.FragmentChatBinding;


public class ChatFragment extends Fragment {


    FragmentChatBinding binding;
    private FragmentTabsPagerAdapter mPagerAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false);






        initControls();
        return binding.getRoot();
    }

    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setupViewPager(binding.viewPager);
    }


    /**
     *  setup view pager
     */
    private void setupViewPager(ViewPager viewPager) {
        mPagerAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());

        ChatActiveFragment activeFragment = new ChatActiveFragment();
        mPagerAdapter.addFragment(activeFragment, getResources().getString(R.string.Cactive));


        viewPager.setAdapter(mPagerAdapter);
        binding.chatTab.setupWithViewPager(viewPager);
        binding.chatTab.setTabMode(1);
    }


}

package pkg.dev.homulacrm.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import pkg.dev.homulacrm.R;
import pkg.dev.homulacrm.databinding.ActivitySplashBinding;
import spencerstudios.com.bungeelib.Bungee;

public class Splash extends AppCompatActivity {

    ActivitySplashBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);





        initControls();

        setTimeOut();
    }

    /**
     *  Method is used to initialized all the controls..
     */
    private void initControls() {

        binding.logoIV.setVisibility(View.GONE);
        setAnimationView(binding.logoIV);
    }

    private void setAnimationView(View topLl) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        topLl.startAnimation(bottomUp);
        topLl.setVisibility(View.VISIBLE);
    }


    /**
     *  Method is used to set time out...
     */
    private void setTimeOut() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(Splash.this, Login.class));
                finish();
                Bungee.slideLeft(Splash.this);
            }
        }, 2400);
    }
}

package pkg.dev.homulacrm.activity.fragment;


import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import pkg.dev.homulacrm.R;
import pkg.dev.homulacrm.activity.Login;
import pkg.dev.homulacrm.activity.interfaces.onBackPressed;
import pkg.dev.homulacrm.activity.viewActivity.HomeActivity;
import pkg.dev.homulacrm.databinding.FragmentSettingsBinding;
import spencerstudios.com.bungeelib.Bungee;


public class SettingsFragment extends Fragment implements View.OnClickListener, onBackPressed {


    FragmentSettingsBinding binding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);






        initControls();
        return binding.getRoot();
    }

    /**
     * Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.topLL.setVisibility(View.GONE);
        setAnimationView(binding.topLL);

        binding.profileLL.setOnClickListener(this);
        binding.helpLL.setOnClickListener(this);
        binding.logoutLL.setOnClickListener(this);
    }


    /**
     *  Method is used to initialized set animation...
     */
    public void setAnimationView(View mMainTopLl) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        mMainTopLl.startAnimation(bottomUp);
        mMainTopLl.setVisibility(View.VISIBLE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.profileLL:
                ((HomeActivity) getActivity()).replaceFragment(new ProfileFragment());
                break;

            case R.id.helpLL:
                ((HomeActivity) getActivity()).replaceFragment(new HelpFragment());
                break;

            case R.id.logoutLL:
                final View dialogView = View.inflate(getActivity(), R.layout.dialog_signout,null);

                final Dialog dialog = new Dialog(getActivity(), R.style.MyAlertDialogStyle);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(dialogView);

                TextView mNoTxt, mYesTxt, mMessageTxt;

                mMessageTxt = dialog.findViewById(R.id.txt_msg);
                mMessageTxt.setText("Are you sure you want to Logout?");

                mNoTxt = dialog.findViewById(R.id.txt_no);
                mYesTxt = dialog.findViewById(R.id.txt_yes);

                mNoTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                mYesTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                       // mAppManager.logoutUser();
                        Intent i = new Intent(getActivity(), Login.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getActivity().startActivity(i);
                        Bungee.slideRight(getActivity());
                    }
                });
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();
               /* if (mAppManager.isUserLoggedIn()) {
                } else {
                }*/
                break;
        }
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}

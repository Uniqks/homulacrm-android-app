package pkg.dev.homulacrm.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pkg.dev.homulacrm.R;
import pkg.dev.homulacrm.activity.adapter.FragmentTabsPagerAdapter;
import pkg.dev.homulacrm.databinding.FragmentClientsBinding;


public class ClientsFragment extends Fragment {


    FragmentClientsBinding binding;
    private FragmentTabsPagerAdapter mPagerAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_clients, container, false);




        initControls();
        return binding.getRoot();
    }

    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setupViewPager(binding.viewPager);
    }


    /**
     *  setup view pager
     */
    private void setupViewPager(ViewPager viewPager) {
        mPagerAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());

        ClientActiveFragment activeFragment = new ClientActiveFragment();
        mPagerAdapter.addFragment(activeFragment, getResources().getString(R.string.Cactive));

        ClientInactiveFragment inactiveFragment = new ClientInactiveFragment();
        mPagerAdapter.addFragment(inactiveFragment, getResources().getString(R.string.Cinactive/*, new Object[]{String.valueOf(5)}*/));

        viewPager.setAdapter(mPagerAdapter);
        binding.clientsTab.setupWithViewPager(viewPager);
        binding.clientsTab.setTabMode(1);
    }


}

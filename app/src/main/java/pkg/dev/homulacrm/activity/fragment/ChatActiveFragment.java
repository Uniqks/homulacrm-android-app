package pkg.dev.homulacrm.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.List;

import pkg.dev.homulacrm.R;
import pkg.dev.homulacrm.activity.viewActivity.HomeActivity;
import pkg.dev.homulacrm.databinding.FragmentChatActiveBinding;
import pkg.dev.homulacrm.model.ChatDetails;


public class ChatActiveFragment extends Fragment {


    FragmentChatActiveBinding binding;
    private List<ChatDetails> mChatDetails = new ArrayList<>();
    private ChatListAdapter mChatListAdapter;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_active, container, false);





        initControls();
        setChatListAdapter();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

    }


    /**
     *  Method is used to Chat list adapter...
     */
    private void setChatListAdapter() {

        this.mChatDetails.add(new ChatDetails("Frank Nava", "Hey ", "02:12 pm", "8", getResources().getDrawable(R.drawable.bpro), "Online"));
        this.mChatDetails.add(new ChatDetails("Hitesh Prajapati", "Hii, How are you?.. ", "11:59 pm", "10+", getResources().getDrawable(R.drawable.apro), "Offline"));
        this.mChatDetails.add(new ChatDetails("Steffy Gracee", "testing dummy message... ", "01 Nov, 2018", "3", getResources().getDrawable(R.drawable.bpro), "Offline"));
        this.mChatDetails.add(new ChatDetails("John ", "testing dummy message... ", "01 Nov, 2018", "20+", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Jack ", "Hello... ", "31 Oct, 2018", "1", getResources().getDrawable(R.drawable.apro), "Online"));
        this.mChatDetails.add(new ChatDetails("Frank Nava", "Hey ", "02:12 pm", "", getResources().getDrawable(R.drawable.bpro), "Online"));
        this.mChatDetails.add(new ChatDetails("Hitesh Prajapati", "Hii, How are you?.. ", "11:59 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("Steffy Gracee", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("John ", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Jack ", "Hello... ", "31 Oct, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Frank Nava", "Hey ", "02:12 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Hitesh Prajapati", "Hii, How are you?.. ", "11:59 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("Steffy Gracee", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("John ", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Jack ", "Hello... ", "31 Oct, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Frank Nava", "Hey ", "02:12 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Hitesh Prajapati", "Hii, How are you?.. ", "11:59 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("Steffy Gracee", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("John ", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Jack ", "Hello... ", "31 Oct, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Frank Nava", "Hey ", "02:12 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Hitesh Prajapati", "Hii, How are you?.. ", "11:59 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("Steffy Gracee", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("John ", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Jack ", "Hello... ", "31 Oct, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Hitesh Prajapati", "Hii, How are you?.. ", "11:59 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("Steffy Gracee", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("John ", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Jack ", "Hello... ", "31 Oct, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Frank Nava", "Hey ", "02:12 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Hitesh Prajapati", "Hii, How are you?.. ", "11:59 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("Steffy Gracee", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("John ", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Jack ", "Hello... ", "31 Oct, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("John ", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Jack ", "Hello... ", "31 Oct, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Frank Nava", "Hey ", "02:12 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Hitesh Prajapati", "Hii, How are you?.. ", "11:59 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("Steffy Gracee", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("John ", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Jack ", "Hello... ", "31 Oct, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Hitesh Prajapati", "Hii, How are you?.. ", "11:59 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("Steffy Gracee", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("John ", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Jack ", "Hello... ", "31 Oct, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Frank Nava", "Hey ", "02:12 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));
        this.mChatDetails.add(new ChatDetails("Hitesh Prajapati", "Hii, How are you?.. ", "11:59 pm", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("Steffy Gracee", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Offline"));
        this.mChatDetails.add(new ChatDetails("John ", "testing dummy message... ", "01 Nov, 2018", "", getResources().getDrawable(R.drawable.placeholderuser), "Online"));


        mChatListAdapter = new ChatListAdapter(mChatDetails, ChatActiveFragment.this);
        binding.chatList.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.chatList.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.list_from_bottom));
        binding.chatList.setAdapter(mChatListAdapter);
    }


    /**
     *  Chat list adapter...
     */
    public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder> {

        private List<ChatDetails> mData;
        private Fragment fragment;


        public ChatListAdapter(List<ChatDetails> mData, Fragment fragment) {
            this.mData = mData;
            this.fragment = fragment;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_chat_list, viewGroup, false);


            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

            if (mData.get(position).getuName() != null && !mData.get(position).getuName().equals("")) {
                holder.mUnameTxt.setText(mData.get(position).getuName());
            } else {
            }

            if (mData.get(position).getMessage() != null && !mData.get(position).getMessage().equals("")) {
                holder.mMessageTxt.setText(mData.get(position).getMessage());
            } else {
            }

            if (mData.get(position).getDateTime() != null && !mData.get(position).getDateTime().equals("")) {
                holder.mDateTxt.setText(mData.get(position).getDateTime());
            } else {
            }

            if (mData.get(position).getMsgCounter() != null && !mData.get(position).getMsgCounter().equals("")) {
                holder.mCountMsgTxt.setText(mData.get(position).getMsgCounter());
            } else {
                holder.mCountMsgTxt.setVisibility(View.GONE);
            }

            if (mData.get(position).getProImg() != null && !mData.get(position).getProImg().equals("")) {

                /*Picasso.get().load(String.valueOf(mData.get(position).getProImg()))
                        .placeholder(R.drawable.placeholderuser)
                        .into(holder.mProImg);*/

                holder.mProImg.setImageDrawable(mData.get(position).getProImg());
            } else {
            }

            if (mData.get(position).getStatus() != null && !mData.get(position).getStatus().equals("")) {

                if (mData.get(position).getStatus().equals("Online")) {
                    holder.mStatusOnImg.setVisibility(View.VISIBLE);
                    holder.mStatusOffImg.setVisibility(View.GONE);
                } else {
                    holder.mStatusOffImg.setVisibility(View.VISIBLE);
                    holder.mStatusOnImg.setVisibility(View.GONE);
                }
            } else {
            }

            holder.mMainLl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((HomeActivity) getActivity()).replaceFragment(new MessagesFragment());
                }
            });

        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView mUnameTxt, mMessageTxt, mDateTxt, mCountMsgTxt;
            RoundedImageView mProImg;
            ImageView mStatusOffImg, mStatusOnImg;
            LinearLayout mMainLl;

            public ViewHolder(@NonNull View view) {
                super(view);

                mUnameTxt = view.findViewById(R.id.txt_uname);
                mMessageTxt = view.findViewById(R.id.txt_message);
                mDateTxt = view.findViewById(R.id.txt_datetime);
                mCountMsgTxt = view.findViewById(R.id.txt_unread_count);
                mProImg = view.findViewById(R.id.img_profile);
                mStatusOffImg = view.findViewById(R.id.img_status_off);
                mStatusOnImg = view.findViewById(R.id.img_status_on);
                mMainLl = view.findViewById(R.id.ll_main);
            }
        }
    }

}

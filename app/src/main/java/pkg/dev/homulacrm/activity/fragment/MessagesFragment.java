package pkg.dev.homulacrm.activity.fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.List;

import pkg.dev.homulacrm.R;
import pkg.dev.homulacrm.activity.interfaces.onBackPressed;
import pkg.dev.homulacrm.databinding.FragmentMessagesBinding;
import pkg.dev.homulacrm.model.MessageDetails;


public class MessagesFragment extends Fragment implements View.OnClickListener, onBackPressed {


    FragmentMessagesBinding binding;
    private List<MessageDetails> mMessageDetails = new ArrayList<>();
    private MessageListAdapter mMessageListAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_messages, container, false);






        initControls();
        setMessageListAdapter();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.backIV.setOnClickListener(this);
        binding.attachIV.setOnClickListener(this);
        binding.sendIV.setOnClickListener(this);

        mMessageDetails.add(new MessageDetails("hiii", getResources().getDrawable(R.drawable.apro), "01 Dec, 2018 3:57 pm",  "receiver"));
        mMessageDetails.add(new MessageDetails("hey", getResources().getDrawable(R.drawable.bpro), "01 Dec, 2018 3:59 pm",  "sender"));
        mMessageDetails.add(new MessageDetails("testing message \n testing \n\n testing", getResources().getDrawable(R.drawable.apro), "01 Dec, 2018 4:00 pm",  "receiver"));
        mMessageDetails.add(new MessageDetails("testing dummy message", getResources().getDrawable(R.drawable.bpro), "01 Dec, 2018 4:00 pm",  "sender"));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backIV:
                hideKey();
                getActivity().getSupportFragmentManager().popBackStack();
                break;

            case R.id.attachIV:
                hideKey();
                Toast.makeText(getActivity(), "Work in progress...", Toast.LENGTH_SHORT).show();
                break;

            case R.id.sendIV:
                hideKey();
                mMessageDetails.add(new MessageDetails(binding.msgET.getText().toString(), getResources().getDrawable(R.drawable.bpro), "02 Nov, 2018 3:57 pm",  "sender"));
                binding.msgET.setText("");
                setMessageListAdapter();
                break;
        }
    }


    /*  hide keyboard  */
    private void hideKey() {
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    /**
     *  Method is used to set message list adapter...
     */
    private void setMessageListAdapter() {
        mMessageListAdapter = new MessageListAdapter(mMessageDetails, MessagesFragment.this);
        binding.msgList.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.msgList.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.list_from_bottom));
        binding.msgList.setAdapter(mMessageListAdapter);
    }



    /**
     * Message list adapter...
     */
    public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.ViewHolder> {

        private List<MessageDetails> mData;
        private Fragment fragment;

        public MessageListAdapter(List<MessageDetails> mData, Fragment fragment) {
            this.mData = mData;
            this.fragment = fragment;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_chat_messages_list, viewGroup, false);



            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

            if (mData.get(position).getMsgStatus() != null && !mData.get(position).getMsgStatus().equals("")) {

                if (mData.get(position).getMsgStatus().equals("receiver")) {
                    holder.mReceiverLl.setVisibility(View.VISIBLE);
                    holder.mSenderLl.setVisibility(View.GONE);


                    if (mData.get(position).getMessage() != null && !mData.get(position).getMessage().equals("")) {
                        holder.mReceiverMsgTxt.setText(mData.get(position).getMessage());
                    } else {
                    }

                    if (mData.get(position).getProImg() != null && !mData.get(position).getProImg().equals("")) {
                        holder.mReceiverImg.setImageDrawable(mData.get(position).getProImg());
                    } else {
                    }

                    if (mData.get(position).getDate() != null && !mData.get(position).getDate().equals("")) {
                        holder.mReceiverTimeTxt.setText(mData.get(position).getDate());
                    } else {
                    }


                } else if (mData.get(position).getMsgStatus().equals("sender")){
                    holder.mReceiverLl.setVisibility(View.GONE);
                    holder.mSenderLl.setVisibility(View.VISIBLE);


                    if (mData.get(position).getMessage() != null && !mData.get(position).getMessage().equals("")) {
                        holder.mSenderMsgTxt.setText(mData.get(position).getMessage());
                    } else {
                    }

                    if (mData.get(position).getProImg() != null && !mData.get(position).getProImg().equals("")) {
                        holder.mSenderImg.setImageDrawable(mData.get(position).getProImg());
                    } else {
                    }

                    if (mData.get(position).getDate() != null && !mData.get(position).getDate().equals("")) {
                        holder.mSenderTimeTxt.setText(mData.get(position).getDate());
                    } else {
                    }

                } else {

                    holder.mReceiverLl.setVisibility(View.GONE);
                    holder.mSenderLl.setVisibility(View.GONE);
                    //other
                }
            } else {
            }
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            LinearLayout mReceiverLl, mSenderLl, mReceiverBubbleLl, mSenderBubbleLl;
            RoundedImageView mReceiverImg, mSenderImg, mReceiverMediaImg, mSenderMediaImg;
            TextView mReceiverMsgTxt, mSenderMsgTxt, mReceiverTimeTxt, mSenderTimeTxt;

            public ViewHolder(@NonNull View view) {
                super(view);

                mReceiverLl = view.findViewById(R.id.ll_receiver);
                mSenderLl = view.findViewById(R.id.ll_sender);
                mReceiverBubbleLl = view.findViewById(R.id.ll_reciver_bubble);
                mSenderBubbleLl = view.findViewById(R.id.ll_sender_bubble);

                mReceiverImg = view.findViewById(R.id.img_receiver);
                mSenderImg = view.findViewById(R.id.img_sender);
                mReceiverMediaImg = view.findViewById(R.id.img_receiver_media);
                mSenderMediaImg = view.findViewById(R.id.img_sender_media);

                mReceiverMsgTxt = view.findViewById(R.id.txt_reciver_msg);
                mSenderMsgTxt = view.findViewById(R.id.txt_sender_msg);
                mReceiverTimeTxt = view.findViewById(R.id.txt_receive_time);
                mSenderTimeTxt = view.findViewById(R.id.txt_sender_time);

            }
        }
    }



    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

}

package pkg.dev.homulacrm.app;

import android.graphics.Color;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.regex.Pattern;





/**
 * Created by Seema on 24/11/18.
 */


public class Validation {

    // Regular Expression
    // you can change the expression based on your need
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_REGEX = "\\d{3}\\d{7}";

    // Error Messages
    private static final String REQUIRED_MSG = "Required";
    private static final String EMAIL_MSG = "Invalid Email Address";
    private static final String PHONE_MSG = "10 digits required";
    private static final String CONFIRM_PASSWORD = "Password Not Match ";

    // call this method when you need to check email validation
    public static boolean isEmailAddress(EditText editText, boolean required, String message) {
        editText.requestFocus();
        return isValid(editText, EMAIL_REGEX, message, required);

    }

    public static boolean isValidPhoneNo(EditText editText, String msg) {
        String text = editText.getText().toString().trim();
        if (text.length() < 10 || text.length() > 14) {
            editText.setError(msg);
            editText.requestFocus();
            //textInputLayout.requestFocus();
            return false;
        }
        return true;

    }

    // call this method when you need to check phone number validation
    public static boolean isPhoneNumber(EditText editText, boolean required) {
        return isValid(editText, PHONE_REGEX, PHONE_MSG, required);
    }

    // return true if the input field is valid, based on the parameter passed
    public static boolean isValid(EditText editText, String regex, String errMsg, boolean required) {

        String text = editText.getText().toString().trim();
        // clearing the error, if it was previously set by some other values
        editText.setError(null);


        // text required and editText is blank, so return false
        if (required && !hasText(editText, errMsg))
            return false;

        // pattern doesn't match so returning false
        if (required && !Pattern.matches(regex, text)) {
            //editText.setError(errMsg);
            Utils.makeToast("" + errMsg, editText.getContext());
            editText.requestFocus();
            return false;
        }
        return true;
    }

    // Edit UserName Validation
    public static boolean isValidString(EditText editText, String error) {
        if (isValidString(getStringFromEditText(editText))) {
            editText.setError(null);
            return true;
        } else {
            editText.setError(error);
            return false;
        }
    }
    public static boolean isValidString(String string) {
        if (string != null && string.length() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public static String getStringFromEditText(EditText editText) {
        return editText.getText().toString();
    }
    // check the input field has any text or not
    // return true if it contains text otherwise false
    public static boolean hasText(EditText editText, String msg) {

        String text = editText.getText().toString().trim();
        editText.setError(null);

        // length 0 means there is no text
        if (text.length() == 0) {
            //editText.setError(msg);
            editText.requestFocus();
            Utils.makeToast("" + msg, editText.getContext());
            //textInputLayout.requestFocus();
            return false;
        }
        return true;
    }

    /*public static boolean hasText1(EditText editText, String message) {

        String text = editText.getText().toString().trim();
        editText.setError(null);

        // length 0 means there is no text
        if (text.length() == 0) {
            editText.setError(message);
            editText.requestFocus();
            return false;
        }
        return true;
    }*/

    public static boolean isValidPassword(EditText editText, String msg) {

        String text = editText.getText().toString().trim();
        editText.setError(null);

        // length 0 means there is no text
        if (text.length() == 0) {
            //editText.setError(msg);
            Utils.makeToast("" + msg, editText.getContext());
            editText.requestFocus();
            return false;
        }
        if (text.length() < 8) {
            Utils.makeToast("" + msg, editText.getContext());
            //editText.setError(msg);
            editText.requestFocus();
            return false;
        }
        return true;
    }


    public static boolean hasSpinnerSelected(Spinner spinner) {

        int position = spinner.getSelectedItemPosition();

        // length 0 means there is no text
        if (position == 0) {
            TextView errorText = (TextView) spinner.getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText("");//changes the selected item text to this
            return false;
        }
        return true;
    }


    public static boolean hasTextToast(EditText editText, String msg) {

        String text = editText.getText().toString().trim();

        editText.clearFocus();
        // length 0 means there is no text
        if (text.length() == 0) {
            editText.requestFocus();

            Utils.makeToast("" + msg, editText.getContext());
            return false;
        }
        return true;
    }

    public static boolean isConfirmPassword(EditText editTextPassword, EditText editTextConfirmPasswords, String message) {

        String password = editTextPassword.getText().toString().trim();
        String confirmPassword = editTextConfirmPasswords.getText().toString().trim();

        if (!password.equals(confirmPassword)) {
            editTextConfirmPasswords.requestFocus();
            Utils.makeToast("" + message, editTextConfirmPasswords.getContext());
            //editTextConfirmPasswords.setError(CONFIRM_PASSWORD);
            return false;
        }
        return true;
    }

}

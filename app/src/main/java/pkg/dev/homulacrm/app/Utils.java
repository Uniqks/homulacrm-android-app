package pkg.dev.homulacrm.app;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import pkg.dev.homulacrm.R;

/**
 * Created by Seema on 24/11/18.
 */

public class Utils {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    static Utils utils;
    Context context;

    public static Geocoder geocoder = null;

    public Utils(Context context) {
        this.context = context;
    }


    public static void goToFragment(Context mContext, Fragment fragment, int container) {
        FragmentTransaction transaction = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment);
        transaction.addToBackStack(fragment.getTag());
        transaction.commitAllowingStateLoss();
    }

    /**
     * set bundle data ...
     */
    public static void goToBundleFragment(Context mContext, Fragment fragment, int container, Bundle bundle) {
        FragmentTransaction transaction = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
        fragment.setArguments(bundle);
        transaction.replace(container, fragment);
        transaction.addToBackStack(fragment.getTag());
        transaction.commitAllowingStateLoss();
    }

    public synchronized static Utils getInstance(Context context) {
        if (utils == null) {
            utils = new Utils(HomulaApp.getGlobalContext());
        }
        return utils;
    }

    public static int getConnectivityStatus(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (null != activeNetwork) {

            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static boolean isNetworkAvailable(Context context) {

        int conn = getConnectivityStatus(context);

        if (conn == TYPE_WIFI) {
            //status = "Wifi enabled";
            //status="Internet connection available";
            return true;
        } else if (conn == TYPE_MOBILE) {
            //status = "Mobile data enabled";
            //status="Internet connection available";
            return true;
        } else if (conn == TYPE_NOT_CONNECTED) {
            //status = "Not connected to Internet";
            return false;
        }
        return false;
    }

    public void Toast(final Activity activity, final String msg, final boolean type) {

        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), msg,
                Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        if (type) {
            snackBarView.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        } else {
            snackBarView.setBackgroundColor(Color.RED);
        }
        snackbar.show();
    }

    public static void makeToast(String message, Context mContext) {
        try {
            Toast toast = Toast.makeText(mContext, "" + message, Toast.LENGTH_SHORT);
            View view = toast.getView();
            //view.setBackgroundResource(R.drawable.toast_bg);
            TextView text = (TextView) view.findViewById(android.R.id.message);
            //text.setTextColor(getColor(mContext, R.color.white));
            toast.setGravity(Gravity.BOTTOM, 0, 190);
            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public File create_Image_File(String ext)
    {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"/"+ context.getString(R.string.app_name));
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date now = new Date();
        // String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault()).format(new Date());
        File mediaFile = new File(fmt.format(now) + ext);

        return mediaFile;
    }

    public File create_file(String ext)
    {
        // File folderPath = new File(Environment.getExternalStorageDirectory() +"/"+ getString(R.string.app_name));
        File shareQr = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!shareQr.exists()) {
            shareQr.mkdirs();
        }
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date now = new Date();
        File newFile =null;//=  File.createTempFile( fmt.format(now) + ".png",shareQr);

        try {
            newFile= File.createTempFile(fmt.format(now).toString(),  /* prefix */ext,         /* suffix */shareQr      /* directory */);

            System.out.println("new File --->" + newFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newFile;
    }

}

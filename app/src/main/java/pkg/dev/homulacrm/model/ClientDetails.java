package pkg.dev.homulacrm.model;

import java.io.Serializable;

/**
 * Created by Seema on 24/11/18.
 */

public class ClientDetails implements Serializable {

    private String uName;
    private String fName;
    private String lName;
    private String email;
    private String phone;
    private String address;
    private String description;
    private String date;


    public ClientDetails(String uName, String fName, String lName, String email, String phone, String address, String description, String date) {
        this.uName = uName;
        this.fName = fName;
        this.lName = lName;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.description = description;
        this.date = date;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}


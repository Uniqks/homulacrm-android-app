package pkg.dev.homulacrm.model;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * Created by Seema on 30/11/18.
 */

public class MessageDetails implements Serializable {

    private String id;
    private String message;
    private String name;
    private Drawable proImg;
    private String mediaUrl;
    private String mediaType;
    private String date;
    private String status;
    private String msgStatus;
    private String toUserId;
    private String fromUserId;

    public MessageDetails(String message, Drawable proImg, String date, String msgStatus) {
        this.message = message;
        this.proImg = proImg;
        this.date = date;
        this.msgStatus = msgStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Drawable getProImg() {
        return proImg;
    }

    public void setProImg(Drawable proImg) {
        this.proImg = proImg;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getMsgStatus() {
        return msgStatus;
    }

    public void setMsgStatus(String msgStatus) {
        this.msgStatus = msgStatus;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }
}

package pkg.dev.homulacrm.model;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * Created by Seema on 24/11/18.
 */

public class ChatDetails implements Serializable {

    private String uName;
    private String message;
    private String dateTime;
    private String msgCounter;
    private Drawable proImg;
    private String status;

    public ChatDetails(String uName, String message, String dateTime, String msgCounter, Drawable proImg, String status) {
        this.uName = uName;
        this.message = message;
        this.dateTime = dateTime;
        this.msgCounter = msgCounter;
        this.proImg = proImg;
        this.status = status;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getMsgCounter() {
        return msgCounter;
    }

    public void setMsgCounter(String msgCounter) {
        this.msgCounter = msgCounter;
    }

    public Drawable getProImg() {
        return proImg;
    }

    public void setProImg(Drawable proImg) {
        this.proImg = proImg;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

